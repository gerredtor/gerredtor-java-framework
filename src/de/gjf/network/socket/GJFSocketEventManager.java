package de.gjf.network.socket;

import de.gjf.event.Event;
import de.gjf.event.Listener;
import de.gjf.module.ApplicationModule;

public class GJFSocketEventManager {

    private final ApplicationModule applicationModule;

    public GJFSocketEventManager(ApplicationModule applicationModule) {
        this.applicationModule = applicationModule;
    }

    public void registerListener(Listener listener) {
        this.applicationModule.getGJF().eventManager.registerGroupedListener(this.getClass().getName(), listener);
    }

    public void removeListener(Listener listener) {
        this.applicationModule.getGJF().eventManager.removeGroupedListener(this.getClass().getName(), listener);
    }

    public void fireEvent(Event event) {
        this.applicationModule.getGJF().eventManager.fireGroupedEvent(this.getClass().getName(), event);
    }


}
