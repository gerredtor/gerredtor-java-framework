package de.gjf.network.socket.client;

import de.gjf.event.Listener;
import de.gjf.module.ApplicationModule;
import de.gjf.network.socket.events.GJFSocketReadEvent;
import de.gjf.network.socket.events.GJFSocketReadFinishEvent;

import java.io.*;
import java.net.Socket;

public class GJFSocket {

    public final Socket socket;

    private BufferedReader bufferedReader = null;
    private BufferedWriter bufferedWriter = null;

    private final ApplicationModule applicationModule;

    public GJFSocket(Socket socket, ApplicationModule applicationModule) {
        this.socket = socket;
        this.applicationModule = applicationModule;
    }

    public void createGJFSocketStreams() {
        try {
            this.bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    int emptyCount = 0;
    boolean listenRun = true;

    public void listenForInpout() {

        emptyCount = 0;

        Thread th = new Thread(() -> {

          //  while (socket.isConnected() && listenRun) {

                try {
                    /*String data = bufferedReader.readLine();

                    if (data.trim().equals("") || data.isEmpty()) {
                        emptyCount++;
                    }

                    if (emptyCount > 3) {
                        applicationModule.getFramework().gjfSocketEventManager.fireEvent(new GJFSocketReadFinishEvent(applicationModule.getGJF()));
                        listenRun = false;
                        break;
                    }

                    if (data != null) {
                        applicationModule.getFramework().gjfSocketEventManager.fireEvent(new GJFSocketReadEvent(applicationModule.getGJF(), data));
                    }*/
                    while (bufferedReader.ready()) {
                        String line = bufferedReader.readLine();
                        applicationModule.getFramework().gjfSocketEventManager.fireEvent(new GJFSocketReadEvent(applicationModule.getGJF(), line));

                    }
                   /* String inLine = null;
                    while (((inLine = bufferedReader.readLine()) != null) && (!(inLine.equals("")))) {
                        applicationModule.getFramework().gjfSocketEventManager.fireEvent(new GJFSocketReadEvent(applicationModule.getGJF(), inLine));
                    }*/

                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            //}

            applicationModule.getFramework().gjfSocketEventManager.fireEvent(new GJFSocketReadFinishEvent(applicationModule.getGJF()));
        });

        th.start();
    }

    public void writeLine(String line) {
        try {
            bufferedWriter.write(line);
            bufferedWriter.newLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void registerListener(Listener listener) {
        this.applicationModule.getFramework().gjfSocketEventManager.registerListener(listener);
    }
}
