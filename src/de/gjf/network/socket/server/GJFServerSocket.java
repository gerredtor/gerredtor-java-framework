package de.gjf.network.socket.server;


import de.gjf.event.Listener;
import de.gjf.module.ApplicationModule;
import de.gjf.network.Webserver.MVC;
import de.gjf.network.socket.client.GJFSocket;
import de.gjf.network.socket.events.GJFServerSocketAcceptEvent;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class GJFServerSocket {

    private final ApplicationModule applicationModule;

    private int port = 0;
    private boolean listenOnlyLocalhost = false;

    private ServerSocket serverSocket = null;

    public GJFServerSocket(ApplicationModule applicationModule) {
        this.applicationModule = applicationModule;
    }

    public void createServerSocket(int port, boolean listenOnlyLocalhost) {
        this.port = port;
        this.listenOnlyLocalhost = listenOnlyLocalhost;

        try {
            this.serverSocket = new ServerSocket(this.port);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void registerListener(Listener listener) {
        this.applicationModule.getFramework().gjfSocketEventManager.registerListener(listener);
    }

    public void listen(MVC mvc) {

        Thread th = new Thread(() -> {

            while (true) {
                try {
                    Socket socket = serverSocket.accept();

                    //Check listen localhost
                    if (listenOnlyLocalhost) {
                        if (!(socket.getInetAddress().toString().equalsIgnoreCase("localhost") || socket.getInetAddress().toString().equalsIgnoreCase("127.0.0.1"))) {
                            socket.close();
                            return;
                        }
                    }

                    applicationModule.getFramework().gjfSocketEventManager.fireEvent(new GJFServerSocketAcceptEvent(applicationModule.getGJF(), socket, mvc));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        });

        th.start();

    }




}
