package de.gjf.network.socket.events;

import de.gjf.GJF;
import de.gjf.event.Event;

import java.util.ArrayList;

public class GJFSocketReadEvent extends Event {

    public final String line;

    public GJFSocketReadEvent(GJF gjf, String line) {
        super(gjf);

        this.line = line;
    }
}
