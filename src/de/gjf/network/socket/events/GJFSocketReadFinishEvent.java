package de.gjf.network.socket.events;

import de.gjf.GJF;
import de.gjf.event.Event;

public class GJFSocketReadFinishEvent extends Event {

    public GJFSocketReadFinishEvent(GJF gjf) {
        super(gjf);
    }
}
