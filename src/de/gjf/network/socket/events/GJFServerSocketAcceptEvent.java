package de.gjf.network.socket.events;

import de.gjf.GJF;
import de.gjf.event.Event;
import de.gjf.network.Webserver.MVC;

import java.net.Socket;

public class GJFServerSocketAcceptEvent extends Event {

    public final Socket socket;
    public final MVC mvc;

    public GJFServerSocketAcceptEvent(GJF gjf, Socket socket, MVC mvc) {
        super(gjf);
        this.socket = socket;
        this.mvc = mvc;
    }


}
