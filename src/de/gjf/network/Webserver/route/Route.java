package de.gjf.network.Webserver.route;

import de.gjf.network.Webserver.model.MVCModel;
import de.gjf.network.Webserver.net.request.RequestType;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

public class Route {

    public String url;
    public RequestType requestType;
    public Controller controllerClass;
    public String methodName;
    public RouteType routeType;

    public HashMap<String, String> routeParameters = new HashMap<>();

    public MVCModel invoke(MVCModel mvcModel) {
        try {
            Controller controller = controllerClass.getClass().newInstance();

            for (Method method : controller.getClass().getMethods()) {
                if (method.getName().equals(this.methodName)) {
                    if (method.getParameterCount() == 0) {
                        return (MVCModel) method.invoke(controller, new Object[]{});
                    } else {
                        return (MVCModel) method.invoke(controller, new Object[]{mvcModel});
                    }
                }
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        return null;
    }

}
