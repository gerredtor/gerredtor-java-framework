package de.gjf.network.Webserver.route;

import de.gjf.network.Webserver.MVC;
import de.gjf.network.Webserver.Manager;
import de.gjf.network.Webserver.net.request.*;
import de.gjf.network.Webserver.util.URLHelper;

import java.util.ArrayList;
import java.util.List;

public class RouteManager extends Manager {

    private ArrayList<Route> routes = new ArrayList<>();

    public RouteManager(MVC mvc) {
        super(mvc);
    }

    public void createRoute(String url, RequestType requestType, Controller controllerClass, String methodName, RouteType routeType) {
        Route route = new Route();
        route.url = url;
        route.requestType = requestType;
        route.controllerClass = controllerClass;
        route.methodName = methodName;
        route.routeType = routeType;
    }

    public Route getRoute(String url, RequestType requestType) {
        List<String> requestUrl = URLHelper.splitUrl(url);

        boolean found = true;

        for (Route route : this.routes) {
            if (route.requestType.equals(requestType)) {

                List<String> routeUrl = URLHelper.splitUrl(route.url);

                if (requestUrl.size() == routeUrl.size()) {

                    //Match Route
                    for (int i = 0; i < routeUrl.size(); i++) {
                        if (!routeUrl.get(i).equalsIgnoreCase(requestUrl.get(i))) {
                            found = false;
                        }
                    }

                    if (found) {
                        return route;
                    }


                    //Sergament Route
                    for (int i = 0; i < routeUrl.size(); i++) {
                        if (!routeUrl.get(i).equalsIgnoreCase(requestUrl.get(i))) {
                            if (!routeUrl.get(i).startsWith(":")) {
                                found = false;
                            } else {
                                String key = routeUrl.get(i).replace(":", "");
                                String value = requestUrl.get(i);
                            }
                        }
                    }

                    if (found) {
                        return route;
                    }
                }
            }
        }

        return null;

    }


}
