package de.gjf.network.Webserver.net;

import de.gjf.event.EventHandler;
import de.gjf.event.Listener;
import de.gjf.module.ApplicationModule;
import de.gjf.network.Webserver.MVC;
import de.gjf.network.Webserver.event.MVCContainerBuilderEvent;
import de.gjf.network.Webserver.net.request.Request;
import de.gjf.network.Webserver.net.request.RequestHelper;
import de.gjf.network.socket.client.GJFSocket;
import de.gjf.network.socket.events.GJFSocketReadEvent;
import de.gjf.network.socket.events.GJFSocketReadFinishEvent;

import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

public class WebClient implements Listener {

    public final GJFSocket gjfSocket;
    private final ApplicationModule applicationModule;
    private final MVC mvc;

    RequestHelper requestHelper = new RequestHelper();

    private ArrayList<String> lines = new ArrayList<>();

    public WebClient(Socket socket, ApplicationModule applicationModule, MVC mvc) {
        this.gjfSocket = new GJFSocket(socket, applicationModule);
        this.applicationModule = applicationModule;
        this.mvc = mvc;

        gjfSocket.registerListener(this);

        gjfSocket.createGJFSocketStreams();
        gjfSocket.listenForInpout();
    }

    @EventHandler
    public void onInput(GJFSocketReadEvent event) {
        this.lines.add(event.line);
        this.watchLine(event.line);
    }

    @EventHandler
    public void onInputFinish(GJFSocketReadFinishEvent event) {
        this.finishListen();
    }

    public void writeLine(String line) {
        this.gjfSocket.writeLine(line);
    }

    private void watchLine(String line) {
        requestHelper.readLine(line);
    }

    private void finishListen() {
        Request request = this.requestHelper.readRequest();
        mvc.fireEvent(new MVCContainerBuilderEvent(applicationModule, request, this));
    }
}
