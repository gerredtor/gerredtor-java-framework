package de.gjf.network.Webserver.net.request;

import de.gjf.network.Webserver.net.parameter.HttpParameter;
import de.gjf.network.Webserver.route.Route;

import java.util.ArrayList;

public class Request {

    public ArrayList<String> headLines;
    public ArrayList<String> contentLines;
    public Route route = new Route();

    public String httpVersion;
    public RequestType requestType;

    //Header and Cookies
    public ArrayList<HttpParameter> httpParameters = new ArrayList<>();

}
