package de.gjf.network.Webserver.net.request;

import de.gjf.network.Webserver.net.parameter.*;

import java.util.ArrayList;

public class RequestHelper {

    private final ArrayList<String> headLines;
    private final ArrayList<String> contentLines;
    private final Request request;

    boolean httpVersionPresent = false;
    boolean contentBegin = false;

    public RequestHelper() {
        this.headLines = new ArrayList<>();
        this.contentLines = new ArrayList<>();
        this.request = new Request();
    }

    public Request readRequest() {
        this.request.headLines = this.headLines;
        this.request.contentLines = this.contentLines;

        return request;
    }

    public void readLine(String line) {
        if (contentBegin) {
            this.contentLines.add(line);
            return;
        }

        this.headLines.add(line);

        if (line.contains("HTTP")) {
            String[] splitted = line.split("/");

            String requestType = splitted[0].trim();
            String httpVersion = splitted[2].trim();

            if (requestType.equals(RequestType.GET)) {
                this.request.requestType = RequestType.GET;
            } else if (requestType.equals(RequestType.POST)) {
                this.request.requestType = RequestType.POST;
            }

            this.request.httpVersion = httpVersion;

            httpVersionPresent = true;

        } else if (!httpVersionPresent) return;

        //Read Headers
        if (line.contains(":")) {
            String[] splitted = line.split(":");

            if (splitted[0].trim().equalsIgnoreCase("cookie")) {
                ArrayList<Cookie> cookies = Cookie.getCookiesFromHeader(splitted[1]);
                this.request.httpParameters.add(Cookie.cookiesToHeader(cookies));
            } else {
                Header header = new Header(splitted[0], splitted[1]);
                this.request.httpParameters.add(header);
            }
        }

        //Wait for empty line
        if (line.trim().equals("")) {
            contentBegin = true;
        }
    }
}
