package de.gjf.network.Webserver.net.parameter;

import java.util.List;

public class HttpParameter {

    public final String key;
    public final Object value;

    public HttpParameter(String key, Object value) {
        this.key = key;
        this.value = value;
    }

    public String buildParameterString() {
        return toString();
    }

    @Override
    public String toString() {
        return this.key + ":" + this.value;
    }

}
