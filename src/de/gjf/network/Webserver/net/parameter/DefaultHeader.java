package de.gjf.network.Webserver.net.parameter;

public enum DefaultHeader {

    header("", "");

    private final String key, value;

    DefaultHeader(String key, String value) {
        this.key = key;
        this.value = value;
    }

    String getKey() { return this.key; }
    String getValue() { return this.value; }

}
