package de.gjf.network.Webserver.net.parameter;

import java.util.ArrayList;

public class Cookie extends HttpParameter {

    public String path = null;
    public boolean secure = false;
    public boolean HttpOnly = false;
    public int MaxAge = 0;
    public String Expires = null;

    public Cookie(String key, Object value, String path, boolean secure, boolean HttpOnly, int MaxAge, String Expires) {
        super(key, value);

        this.path = path;
        this.secure = secure;
        this.HttpOnly = HttpOnly;
        this.MaxAge = MaxAge;
        this.Expires = Expires;
    }

    @Override
    public String toString() {
        return this.key + "=" + this.value;
    }

    public static ArrayList<Cookie> getCookiesFromHeader(String line) {
        ArrayList<Cookie> cookies = new ArrayList<>();

        if (line.contains(";")) {
            String[] singleCookieData = line.split(";");

            for (String cookieData : singleCookieData) {
                if (cookieData.contains("=")) {
                    String[] cookieKeyValue = cookieData.split("=");

                    cookies.add(Cookie.simpleCookie(cookieKeyValue[0], cookieKeyValue[1]));
                }
            }
        }

        return cookies;
    }

    public static Header cookiesToHeader(ArrayList<Cookie> cookies) {
        String key = "Cookie";
        String value = "";

        for (Cookie cookie : cookies) {
            value += cookie.toString() + ";";
        }

        return new Header(key, value);
    }

    public static Cookie simpleCookie(String key, Object value) {
        return new Cookie(key, value, null, false, false, 0, null);
    }

}
