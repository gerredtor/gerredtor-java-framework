package de.gjf.network.Webserver.net.parameter;

public class Header extends HttpParameter {

    public Header(String key, Object value) {
        super(key, value);
    }

    @Override
    public String toString() {
        return this.key + ": " + this.value;
    }

}
