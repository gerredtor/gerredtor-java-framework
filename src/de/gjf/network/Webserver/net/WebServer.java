package de.gjf.network.Webserver.net;

import de.gjf.event.EventHandler;
import de.gjf.event.Listener;
import de.gjf.module.ApplicationModule;
import de.gjf.network.Webserver.MVC;
import de.gjf.network.socket.events.GJFServerSocketAcceptEvent;
import de.gjf.network.socket.server.GJFServerSocket;

import java.util.ArrayList;

public class WebServer implements Listener {

    private GJFServerSocket gjfServerSocket = null;

    private ArrayList<WebClient> webClients = new ArrayList<>();

    private final ApplicationModule applicationModule;
    private final MVC mvc;

    public WebServer(ApplicationModule applicationModule, MVC mvc) {
        this.applicationModule = applicationModule;
        this.mvc = mvc;
    }

    public void createWebServer() {
        this.gjfServerSocket = new GJFServerSocket(this.applicationModule);
        this.gjfServerSocket.createServerSocket(80, false);

        this.gjfServerSocket.registerListener(this);

        this.gjfServerSocket.listen(mvc);
    }

    @EventHandler
    public void onConnection(GJFServerSocketAcceptEvent event) {
        webClients.add(new WebClient(event.socket, applicationModule, event.mvc));
    }

    public void writeLineToAll(String line) {

        for (WebClient webClient : this.webClients) {
            if (webClient.gjfSocket.socket.isConnected()) {
                webClient.writeLine(line);
            }
        }

    }


}
