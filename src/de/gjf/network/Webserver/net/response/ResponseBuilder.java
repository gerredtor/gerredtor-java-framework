package de.gjf.network.Webserver.net.response;

import de.gjf.network.Webserver.model.MVCModel;
import de.gjf.network.Webserver.net.parameter.Header;

import java.util.ArrayList;

public class ResponseBuilder {

    private ArrayList<String> headerLines = new ArrayList<>();
    private ArrayList<String> contentLines = new ArrayList<>();

    private final MVCModel mvcModel;
    private final Response response;

    public ResponseBuilder(MVCModel mvcModel) {
        this.mvcModel = mvcModel;
        this.response = new Response();
    }

    public void addHeader(Header header) {
        this.headerLines.add(header.toString());
    }

    public void addHeaders(ArrayList<Header> headers) {
        for (Header header : headers) {
            this.addHeader(header);
        }
    }

    public void setContent(ArrayList<String> lines) {
        this.contentLines = lines;
    }

    public Response getResponse() {
        this.response.headerLines = this.headerLines;
        this.response.contentLines = this.contentLines;

        return this.response;
    }

    public static ArrayList<Header> getDefaultHeader() {
        ArrayList<Header> header = new ArrayList<>();

        header.add(new Header("content-type", "application/json"));

        return header;
    }
}
