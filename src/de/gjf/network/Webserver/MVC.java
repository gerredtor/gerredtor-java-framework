package de.gjf.network.Webserver;

import de.gjf.module.ApplicationModule;
import de.gjf.network.Webserver.event.MVCContainerBuilderEvent;
import de.gjf.network.Webserver.model.MVCModel;
import de.gjf.network.Webserver.net.WebServer;
import de.gjf.network.Webserver.net.request.Request;
import de.gjf.network.Webserver.net.request.RequestType;
import de.gjf.network.Webserver.net.response.ResponseBuilder;
import de.gjf.network.Webserver.route.Route;
import de.gjf.network.Webserver.route.RouteManager;
import de.gjf.event.*;
import de.gjf.network.Webserver.route.RouteType;
import mvcTest.TestController;

public class MVC implements Listener {

    public final RouteManager routeManager;
   // public final ModuleManager moduleManager;

    public final ApplicationModule applicationModule;

    private WebServer webServer = null;

    //Listener Group
    private final String MVC_LISTENER_GROUP = "MVC_LISTENER_GROUP";

    public MVC(ApplicationModule applicationModule) {
        this.routeManager = new RouteManager(this);
        //this.moduleManager = new ModuleManager(this);

        this.applicationModule = applicationModule;

        registerListener(this);

        routeManager.createRoute("/", RequestType.GET, new TestController(), "toIndex", RouteType.MATCH_ROUTE);
    }

    public void startMVC() {
        this.webServer = new WebServer(this.applicationModule, this);
        this.webServer.createWebServer();
    }

    public void registerListener(Listener listener) {
        this.applicationModule.getGJF().eventManager.registerGroupedListener(this.getClass().getName(), listener);
    }

    public void removeListener(Listener listener) {
        this.applicationModule.getGJF().eventManager.removeGroupedListener(this.getClass().getName(), listener);
    }

    public void fireEvent(Event event) {
        this.applicationModule.getGJF().eventManager.fireGroupedEvent(this.getClass().getName(), event);
    }



        @EventHandler
        public void onMVCEventBuilderEvent(MVCContainerBuilderEvent event) {
            MVCModel mvcContainer = new MVCModel(event.request, event.response);

            Route route = routeManager.getRoute("/", RequestType.GET);

            if (route == null) {
                //404
            }

            mvcContainer.request.route = route;

            MVCModel mvcModel = route.invoke(mvcContainer);

            ResponseBuilder responseBuilder = new ResponseBuilder(mvcModel);
            responseBuilder.addHeaders(ResponseBuilder.getDefaultHeader());
            responseBuilder.setContent(responseBuilder.getResponse().contentLines);

            mvcModel.response = responseBuilder.getResponse();

            //Send data to client
            for (String line : mvcModel.response.headerLines) {
                event.webClient.writeLine(line);
            }

            event.webClient.writeLine("");

            for (String line : mvcModel.response.contentLines) {
                event.webClient.writeLine(line);
            }

    }

}
