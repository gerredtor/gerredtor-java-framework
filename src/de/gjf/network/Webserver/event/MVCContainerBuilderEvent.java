package de.gjf.network.Webserver.event;

import de.gjf.event.Event;
import de.gjf.module.ApplicationModule;
import de.gjf.network.Webserver.net.WebClient;
import de.gjf.network.Webserver.net.request.Request;
import de.gjf.network.Webserver.net.response.Response;

public class MVCContainerBuilderEvent extends Event {

    public final Request request;
    public final Response response;

    public final WebClient webClient;

    public MVCContainerBuilderEvent(ApplicationModule applicationModule, Request request, WebClient webClient) {
        super(applicationModule.getGJF());

        this.request = request;
        this.response = new Response();

        this.webClient = webClient;
    }

}
