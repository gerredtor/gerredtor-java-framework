package de.gjf.network.Webserver;

import de.gjf.network.Webserver.net.request.RequestType;
import de.gjf.network.Webserver.route.Route;

public abstract class Manager {

    private final MVC mvc;

    public Manager(MVC mvc) {
        this.mvc = mvc;
    }
}
