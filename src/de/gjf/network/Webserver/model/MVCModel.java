package de.gjf.network.Webserver.model;

import de.gjf.network.Webserver.model.view.View;
import de.gjf.network.Webserver.net.request.Request;
import de.gjf.network.Webserver.net.response.Response;


public class MVCModel {

    public Request request = null;
    public Response response = null;

    public ViewModel viewModel = null;
    public View view = null;

    public MVCModel(Request request, Response response) {
        this.request = request;
        this.response = response;
    }

    public MVCModel assingViewModel(ViewModel viewModel) {
        this.viewModel = viewModel;

        return this;
    }
}
