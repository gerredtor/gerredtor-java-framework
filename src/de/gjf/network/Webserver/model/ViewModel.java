package de.gjf.network.Webserver.model;

import java.util.ArrayList;
import java.util.Map;

public abstract class ViewModel {

    protected final ArrayList<String> lines;
    protected final Map<Object, Object> putMap;

    public ViewModel(Map<Object, Object> map) {
        this.lines = new ArrayList<>();
        this.putMap = map;
    }

    public abstract String[] getContentLines();


}
