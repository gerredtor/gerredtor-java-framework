package de.gjf.network.Webserver.model.view;

import com.google.gson.Gson;
import de.gjf.network.Webserver.model.ViewModel;

import java.util.Map;

public class JsonModel extends ViewModel {

    public JsonModel(Map<Object, Object> map) {
        super(map);
    }

    @Override
    public String[] getContentLines() {
        Gson gson = new Gson();
        return new String[]{gson.toJson(putMap)};
    }
}
