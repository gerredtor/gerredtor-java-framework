package de.gjf.network.Webserver.util;

import java.util.Arrays;
import java.util.List;

public class URLHelper {

    public static List<String> splitUrl(String url) {
        return Arrays.asList(url.split("/"));
    }

}
