package de.gjf.module;

import de.gjf.GJF;
import de.gjf.command.CommandManager;
import de.gjf.cron.CronJobManager;
import de.gjf.event.EventManager;
import de.gjf.log.LogManager;
import de.gjf.network.socket.GJFSocketEventManager;
import de.gjf.schedule.ScheduleManager;

public abstract class Framework extends ApplicationModule {

    public final ModuleManager moduleManager;
    public final EventManager eventManager;
    public final CommandManager commandManager;
    public final ScheduleManager scheduleManager;
    public final LogManager logManager;
    public final CronJobManager cronJobManager;
    public final GJFSocketEventManager gjfSocketEventManager;

    private final GJF gjf;

    public Framework() {
        this.gjf = (GJF) this;

        this.moduleManager = new ModuleManager(this);
        this.eventManager = new EventManager(this);
        this.commandManager = new CommandManager(this);
        this.scheduleManager = new ScheduleManager(this);
        this.logManager = new LogManager(this);
        this.cronJobManager = new CronJobManager(this);
        this.gjfSocketEventManager = new GJFSocketEventManager(this);
    }

    @Override
    public GJF getGJF() {
        return this.gjf;
    }
}
