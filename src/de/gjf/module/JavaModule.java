package de.gjf.module;

import de.gjf.GJF;
import de.gjf.configuration.Configuration;
import de.gjf.configuration.types.JsonConfiguration;

import java.io.File;

public class JavaModule extends ApplicationModule {

    private Module module = null;
    private Framework framework = null;

    public JavaModule() {

    }

    protected void setModule(Module module) {
        if (this.module == null) {
            this.module = module;
        }
    }

    protected void setFramework(Framework framework) {
        if (this.framework == null) {
            this.framework = framework;
        }
    }

    public Module getModule() {
        return this.module;
    }
    public Framework getFramework() {
        return this.framework;
    }

    public void moduleOn() {}
    public void moduleOff() {}

    @Override
    public GJF getGJF() {
        return (GJF) this.framework;
    }

    public void stopManager() {
        framework.scheduleManager.stop(this);
    }

    public Configuration getModuleConfiguration(String filename) {
        return new JsonConfiguration(new File("modules/" + this.module.moduleIdentifier + "/" + filename));
    }

}
