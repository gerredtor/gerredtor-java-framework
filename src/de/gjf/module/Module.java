package de.gjf.module;

public class Module {

    public final JavaModule javaModule;
    public final ClassLoader classLoader;
    public final String moduleIdentifier;
    public final String moduleName;

    public Module(JavaModule javaModule, ClassLoader classLoader, String moduleIdentifier, String moduleName) {
        this.javaModule = javaModule;
        this.classLoader = classLoader;
        this.moduleIdentifier = moduleIdentifier;
        this.moduleName = moduleName;
    }


}
