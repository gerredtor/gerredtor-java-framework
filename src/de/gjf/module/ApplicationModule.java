package de.gjf.module;

import de.gjf.GJF;

public abstract class ApplicationModule {

    public abstract GJF getGJF();

    public Framework getFramework() {
        return this.getGJF();
    }

}
