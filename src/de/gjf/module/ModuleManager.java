package de.gjf.module;

import java.util.ArrayList;

public class ModuleManager {

    private ArrayList<JavaModule> loadedModules = new ArrayList<>();
    private ArrayList<Module> registredModules = new ArrayList<>();

    private final ApplicationModule applicationModule;

    public ModuleManager(ApplicationModule applicationModule) {
        this.applicationModule = applicationModule;
    }

    public void registerModule(Module module) {
        this.registredModules.add(module);
    }

    public boolean loadModules() {
        //ModuleLoader
        ModuleLoader moduleLoader = new ModuleLoader(this.applicationModule.getGJF());
        moduleLoader.loadExternalModules();

        for (Module module : this.registredModules) {
            //Create JavaModule Instance
            Class c = module.javaModule.getClass();

            JavaModule javaModule = null;
            try {
                javaModule = (JavaModule) c.newInstance();

                //Inject Module Info
                javaModule.setModule(module);

                //Inject Main Module
                javaModule.setFramework((Framework) this.applicationModule);
            } catch (InstantiationException e) {
                e.printStackTrace();
                return false;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                return false;
            }

            //Register in loadedModules
            this.loadedModules.add(javaModule);

            //Start Module
            javaModule.moduleOn();
        }

        return true;
    }

    public void stopModule(JavaModule javaModule) {
        //Stop Manager
        javaModule.stopManager();

        //Stop Module
        javaModule.moduleOff();

        //Unload Module
        this.loadedModules.remove(javaModule);
    }

    public void stopModule(String moduleIdentifier) {
        for (JavaModule javaModule : this.loadedModules) {
            if (javaModule.getModule().moduleIdentifier.equals(moduleIdentifier)) {
                this.stopModule(javaModule);
            }
        }
    }

}
