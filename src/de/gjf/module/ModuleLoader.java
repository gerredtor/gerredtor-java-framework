package de.gjf.module;

import de.gjf.GJF;
import de.gjf.configuration.Configuration;
import de.gjf.configuration.ConfigurationManager;
import de.gjf.configuration.types.JsonConfiguration;
import de.gjf.util.ZipUtils;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ModuleLoader {

    private final GJF gjf;

    private ArrayList<Module> modules = new ArrayList<>();

    public ModuleLoader(GJF gjf) {
        this.gjf = gjf;
    }

    public void loadExternalModules() {
        String moduleDir = this.gjf.getENV().getString("moduleDir");

        if (moduleDir.equals("") || moduleDir == null) {
            moduleDir = "modules";
        }

        List<String> jarFiles = new ArrayList<>();

        try (Stream<Path> walk = Files.walk(Paths.get(moduleDir))) {
            jarFiles = walk.map(x -> x.toString())
                    .filter(f -> f.endsWith(".jar")).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (jarFiles.size() == 0) {
            return;
        }

        for (String file : jarFiles) {

            Configuration moduleConfiguration = this.verifyExternalModule(new File(file));

            if (moduleConfiguration != null) {
                try {
                    Module module = this.loadClasses(new File(file), moduleConfiguration);
                    gjf.moduleManager.registerModule(module);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private Configuration verifyExternalModule(File file) {
        //Load module.json from module.jar
        byte[] filebytes = null;

        try {
            filebytes = ZipUtils.getFileBytes(new FileInputStream(file), "module.json");
            System.out.println(new String(filebytes));
        } catch (Exception e) {
            return null;
        }

        return ConfigurationManager.createConfiguration(new JsonConfiguration(filebytes));
    }

    private Module loadClasses(File jar, Configuration moduleConfiguration) throws IOException {
        JarInputStream jaris = new JarInputStream(new FileInputStream(jar));

        JarEntry ent = null;

        JavaModule mainClass = null;

        ArrayList<File> files = new ArrayList<>();

        while ((ent = jaris.getNextJarEntry()) != null) {

            if (ent.getName().toLowerCase().endsWith(".class")) {


                    files.add(new File(ent.getName()));





            }

        }

        //Load Classes
        File[] plugJars = new File[]{ jar };

        ClassLoader cl = new URLClassLoader(fileArrayToURLArray(plugJars));
        for (File file : files) {
                try

                {


                        Class<?> cls = cl.loadClass(file.getName().substring(0, file.getName().length() - 6).replace('/', '.'));

                        if (cls.getSuperclass().getTypeName().equals(JavaModule.class.getTypeName())) {
                            try {
                                mainClass = (JavaModule) cls.newInstance();
                            } catch (InstantiationException e) {
                                e.printStackTrace();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }

                    } catch(ClassNotFoundException e){

                    System.err.println("Can't load Class " + ent.getName());

                    e.printStackTrace();

                }
                }
        jaris.close();

        String moduleIdentifier = moduleConfiguration.getString("module_identifier");
        String moduleName = moduleConfiguration.getString("module_name");

        return new Module(mainClass, cl, moduleIdentifier, moduleName);

    }


    private static URL[] fileArrayToURLArray(File[] files) throws MalformedURLException {

        URL[] urls = new URL[files.length];

        for (int i = 0; i < files.length; i++) {

            urls[i] = files[i].toURI().toURL();

        }

        return urls;
    }




}
