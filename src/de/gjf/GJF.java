package de.gjf;

import java.io.File;

import de.gjf.command.CommandManager;
import de.gjf.configuration.ConfigurationManager;
import de.gjf.configuration.ConfigurationOptions;
import de.gjf.configuration.types.JsonConfiguration;
import de.gjf.console.Console;
import de.gjf.database.Database;
import de.gjf.database.MysqlData;
import de.gjf.event.EventManager;
import de.gjf.module.Framework;
import de.gjf.network.Webserver.MVC;
import de.gjf.util.FileHelper;

public class GJF extends Framework {

	private final String ENV_FILE;
	
	private final JsonConfiguration env;
	private MysqlData mysqlData = null;
	private Console console = null;



	private ApplicationLoop applicationLoop = null;
	
	public GJF(ConfigurationOptions configurationOptions) {
		super();

		//set ENV_FILE
		this.ENV_FILE = configurationOptions.envFile;

		//Start Programm loop
		this.applicationLoop = new ApplicationLoop();
		new Thread(this.applicationLoop).start();

		this.console = new Console(this);
		
		if (!new File(this.ENV_FILE).exists()) {
			if (configurationOptions.copyDefaults) {
				configurationOptions.gjf = this;
				FileHelper fh = new FileHelper();
				fh.copyFileFromResources(configurationOptions.fileDefaultUrl, this.ENV_FILE);
			} else {
				configurationOptions.forceWriteEmptyJsonFile = false;
				configurationOptions.file = this.ENV_FILE;
			}
		}
		
		this.env = (JsonConfiguration) ConfigurationManager.createConfiguration(new JsonConfiguration(new File(this.ENV_FILE)), configurationOptions);
		
		/*try {
		
			if (this.env.get("database").getString("host") != null &&
					this.env.get("database").getString("username") != null &&
							this.env.get("database").getString("password") != null &&
									this.env.get("database").getString("database") != null) {
				this.mysqlData = new MysqlData();
				this.mysqlData.host = this.env.get("database").getString("host");
				this.mysqlData.username = this.env.get("database").getString("username");
				this.mysqlData.password = this.env.get("database").getString("password");
				this.mysqlData.database = this.env.get("database").getString("database");
				
				if (this.env.get("database").getInt("port") > 0) {
					this.mysqlData.port = this.env.get("database").getInt("port");
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}*/

		//Load Modules
		this.moduleManager.loadModules();

		//Loop Crons
		//this.cronJobManager.loopCrons();

		//MVC

		MVC mvc = new MVC(this);
		mvc.startMVC();



	}
	
	public JsonConfiguration getENV() {
		return this.env;
	}
	
	public Database getDB() {
		return new Database(this.mysqlData.host, this.mysqlData.username, this.mysqlData.password, this.mysqlData.database);
	}
	
	public EventManager getEventManager() {
		return this.eventManager;
	}

	public CommandManager getCommandManager() {
		return this.commandManager;
	}
	
	public Console getConsole() {
		return this.console;
	}
	
	class ApplicationLoop implements Runnable {

		private boolean loop = true;
		
		@Override
		public void run() {
			while(loop) { }
		}
		
		public void stopApplication() {
			this.loop = false;
		}
		
	}
}
