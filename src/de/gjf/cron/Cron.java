package de.gjf.cron;

import com.google.gson.annotations.Expose;
import de.gjf.cron.event.CronJobEvent;
import de.gjf.event.Event;
import de.gjf.module.ApplicationModule;

import java.util.Date;

public class Cron {

    @Expose
    public long lastScheduleMilliseconds = 0;

    @Expose
    public long scheduleMilliseconds = 0;

    @Expose
    public String eventName = "";

    public Cron() {}

    public void run(ApplicationModule applicationModule, CronJobManager cjm) {
        Event event = applicationModule.getFramework().eventManager.fireGroupedEvent(this.eventName, new CronJobEvent(applicationModule.getGJF()));

        if (event.isCanceld()) {
            return;
        }

        Date date = new Date();
        long time = date.getTime();

        this.lastScheduleMilliseconds = time;

        cjm.saveCrons();
    }

}
