package de.gjf.cron;

import com.mysql.jdbc.StringUtils;
import de.gjf.configuration.Configuration;
import de.gjf.configuration.types.JsonConfiguration;
import de.gjf.module.ApplicationModule;
import de.gjf.schedule.Schedule;
import de.gjf.util.TimeHelper;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static org.springframework.util.ClassUtils.getShortName;

public class CronJobManager {

    private List<Cron> crons = new ArrayList<Cron>();

    public final ApplicationModule applicationModule;

    private Configuration cronConfiguration = null;

    public CronJobManager(ApplicationModule applicationModule) {
        this.applicationModule = applicationModule;

        this.cronConfiguration = new JsonConfiguration(new File("cronJobs.json"));
    }

    public void loopCrons() {
        this.loop();
    }

    public void registerCron(ApplicationModule applicationModule, Cron cron) {
        this.crons.add(cron);

        this.saveCrons();
    }

    private void loadCrons() throws Exception {
       if (this.cronConfiguration.get("crons") == null) {
            this.cronConfiguration.set("crons", this.cronConfiguration.serialization(this.crons));
            this.cronConfiguration.save();
        }

        List<Cron> crons = (List<Cron>) this.cronConfiguration.deserializationArray(this.cronConfiguration.get("crons").toString(), Cron[].class);

        this.crons = (List<Cron>) crons;

        if (this.crons == null) {
            this.crons = new ArrayList<>();
        }
    }

    protected void saveCrons() {
        this.cronConfiguration.set("crons", this.cronConfiguration.serialization(this.crons));
        this.cronConfiguration.save();
    }

    private void loop() {
        Schedule.runAsyncDelayTimed(this.applicationModule, () -> {
            try {
                this.loadCrons();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (this.crons == null || this.crons.size() == 0) {
                return;
            }

            for (Cron cron : this.crons) {
                if (cron.lastScheduleMilliseconds == 0) {
                    cron.run(this.applicationModule, this);
                } else if ((TimeHelper.getTimestamp() - cron.lastScheduleMilliseconds) > cron.scheduleMilliseconds) {
                    cron.run(this.applicationModule, this);
                }
            }
        }, 60, 600);
    }

}
