package de.gjf.exception.event;

import de.gjf.GJF;
import de.gjf.event.Event;

public class GJFExceptionEvent extends Event {

    public GJFExceptionEvent(GJF gjf) {
        super(gjf);
    }
}
