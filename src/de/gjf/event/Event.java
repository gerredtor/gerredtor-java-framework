package de.gjf.event;

import de.gjf.GJF;

public class Event extends Cancelable {

	private final GJF gjf;
	
	public Event(GJF gjf) {
		this.gjf = gjf;
	}
	
	public GJF GJF() {
		return this.gjf;
	}
	
}
