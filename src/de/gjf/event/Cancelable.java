package de.gjf.event;

public class Cancelable {

    private boolean cancel = false;

    public void setCancel(boolean cancel) {
        this.cancel = cancel;
    }

    public boolean isCanceld() {
        return this.cancel;
    }

}
