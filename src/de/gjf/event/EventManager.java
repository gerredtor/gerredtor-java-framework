package de.gjf.event;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

import de.gjf.module.ApplicationModule;

public class EventManager {

	private final ApplicationModule applicationModule;
	
	public EventManager(ApplicationModule applicationModule) {
		this.applicationModule = applicationModule;
	}

	private final String MAIN_GROUP = "MAIN_GROUP";

	private HashMap<String, ArrayList<Listener>> groupedListeners = new HashMap<>();

	//Public Listener
	public Event fireEvent(Event event) {
		return this.fireGroupedEvent(MAIN_GROUP, event);
	}

	public Event fireEventAsync(Event event) {
		return this.fireGroupedEventAsync(MAIN_GROUP, event);
	}

	public void registerListener(Listener listener) {
		this.registerGroupedListener(MAIN_GROUP, listener);
	}

	public void removeListener(Listener listener) {
		this.removeGroupedListener(MAIN_GROUP, listener);
	}

	//Grouped Listener
	public Event fireGroupedEvent(String group, Event event) {
		return this.fireGrouped(group, event, false);
	}


	public Event fireGroupedEventAsync(String group, Event event) {
		return this.fireGrouped(group, event, true);
	}


	public void registerGroupedListener(String group, Listener listener) {
		this.initEmptyGroup(group);

		this.groupedListeners.get(group).add(listener);
	}

	public void removeGroupedListener(String group, Listener listener) {
		this.initEmptyGroup(group);

		this.groupedListeners.get(group).remove(listener);
	}

	private void initEmptyGroup(String group) {
		if (this.groupedListeners.get(group) == null) {
			this.groupedListeners.put(group, new ArrayList<Listener>());
		}
	}

	private Event fireGrouped(String group, Event event, boolean async) {
		final Event[] data = {null};

		for (Listener listener : this.groupedListeners.get(group)) {
			if (event.isCanceld()) {
				break;
			}

			for (Method method : listener.getClass().getMethods()) {

				if (event.isCanceld()) {
					break;
				}

				if (method.getParameterCount() == 1 &&
						method.getParameterTypes()[0].equals(event.getClass()) &&
						method.isAnnotationPresent(EventHandler.class)) {

					try {
						if (async) {
							final Event[] tmpData = {null};

							Thread th = new Thread(() -> {
								try {
									tmpData[0] = (Event) method.invoke(listener, new Object[]{event});
								} catch (IllegalAccessException e) {
									e.printStackTrace();
								} catch (InvocationTargetException e) {
									e.printStackTrace();
								}
							});

							th.start();
							th.interrupt();

							data[0] = tmpData[0];
						} else {
							data[0] = (Event) method.invoke(listener, new Object[]{event});
						}
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}
				}
			}
		}

		return data[0];
	}
}
