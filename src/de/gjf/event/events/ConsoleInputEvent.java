package de.gjf.event.events;

import de.gjf.event.Event;

public class ConsoleInputEvent extends Event {
	
	protected final String input;
	protected final String cmd;
	protected final String[] args;
	
	public ConsoleInputEvent(String input, de.gjf.GJF gjf) {
		super(gjf);
		
		this.input = input;
		
		//Extract cmd and arg from input
		String[] inputArr = input.split(" ");
		
		this.cmd = inputArr[0];
		inputArr[0] = null;
		this.args = inputArr;
		
		//call ConsoleCommandManager
		//this.GJF().getCommandManager().checkConsoleInput(input);
		this.GJF().getCommandManager().checkConsoleInputAsync(input);
	}
	
}
