package de.gjf.log;

import de.gjf.module.ApplicationModule;

public class Log {

    public static void log(ApplicationModule applicationModule, LogLevel logLevel, LogData logData, boolean consoleOutput) {

        String logMessageHead = "[" + logLevel.toString() + "|" + logData.title + "]";
        String logMessageContent = logData.message;
        String logMessageBody = "executed from module " + applicationModule.getClass().getSimpleName() + " in " + logData.file;

        if (consoleOutput) {
            System.out.println(logMessageHead);
            System.out.println(logMessageContent);
            System.out.println(logMessageBody);
        }
    }

}
