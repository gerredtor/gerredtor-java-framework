package de.gjf.log;

public enum LogLevel {

    INFO, WARNING, ERROR

}
