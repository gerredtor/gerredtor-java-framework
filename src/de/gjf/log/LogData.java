package de.gjf.log;

public class LogData {

    public final String message,title,file;

    public LogData(String message, String title, String file) {
        this.message = message;
        this.title = title;
        this.file = file;
    }

}
