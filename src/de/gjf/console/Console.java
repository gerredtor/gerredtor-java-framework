package de.gjf.console;

import java.util.Scanner;

import de.gjf.GJF;
import de.gjf.event.events.ConsoleInputEvent;

public class Console {

	private Runnable listenRunnable = null;
	private Thread listenThread = null;
	
	private Scanner scanner = new Scanner(System.in);
	
	private final GJF gjf;
	
	public Console(GJF gjf) {
		this.gjf = gjf;
	}

	@SuppressWarnings("deprecation")
	public void listenForInput(boolean listen) {
		if (listen) {
			this.listenRunnable = new InputListen();
			this.listenThread = new Thread(this.listenRunnable);
			this.listenThread.start();
		} else {
			if (this.listenThread != null) {
				this.listenThread.interrupt();
				this.listenThread.stop();
				this.listenThread.destroy();
				this.listenThread = null;
				this.listenRunnable = null;
			}
		}
	}
	
	class InputListen implements Runnable {
	
		@Override
		public void run() {
			while (listenThread != null) {
				System.out.print("> ");
				String input = scanner.nextLine();
				gjf.getEventManager().fireEvent(new ConsoleInputEvent(input, gjf));
			}
		}
		
	}
}
