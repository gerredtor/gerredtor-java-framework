package de.gjf.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.springframework.util.ResourceUtils;

import de.gjf.configuration.types.JsonConfiguration;

public class FileHelper {

	public static String fileToString(File file) throws IOException {
		return new String(Files.readAllBytes(Paths.get(file.toURI())));
	}

	public static String getFileFromResources(String localFile) throws IOException {
		File file = null;

		try {
			file = ResourceUtils.getFile("classpath:"+localFile);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return FileHelper.fileToString(file);
	}

	public void copyFileFromResources(String resourceName, String destination) {
		//File file = new File(getClass().getClassLoader().getResource(resourceName).getFile());
		File file = null;
		
		try {
			file = ResourceUtils.getFile("classpath:"+resourceName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		try {
			file = ResourceUtils.getFile(resourceName);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		JsonConfiguration jsonConfiguration = new JsonConfiguration(file);
		jsonConfiguration.save(new File(destination));
	}
	
	public void writeEmptyJsonFile(String file) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(file));
	    writer.write("{}");
	    writer.close();
	}

	public void writeFile(String file, String content) throws IOException {
		BufferedWriter writer = new BufferedWriter(new FileWriter(file));
		writer.write(content);
		writer.close();
	}
	
}
