package de.gjf.util;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class Scheduler {

	public static void schedule(TimerTask timerTask, int delay) {
		Timer timer = new Timer();
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				timer.schedule(timerTask, delay);		
			}
		}).start();
	}
	
	public static void scheduleLoop(TimerTask timerTask, int firstDelay, int secondDelay) {
		Timer timer = new Timer();
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					Thread.sleep(firstDelay);
					
					timer.scheduleAtFixedRate(timerTask, new Date(), secondDelay);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}				
			}
		}).start();		
	}
}
