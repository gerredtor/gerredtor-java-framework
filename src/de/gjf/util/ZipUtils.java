package de.gjf.util;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipUtils {

    /**
     * @param source zip stream
     * @param target target directory
     * @throws IOException extraction failed
     */
    public static void unzip(InputStream source, File target) throws IOException {
        final ZipInputStream zipStream = new ZipInputStream(source);
        ZipEntry nextEntry;
        while ((nextEntry = zipStream.getNextEntry()) != null) {
            final String name = nextEntry.getName();
            // only extract files
            if (!name.endsWith("/")) {
                final File nextFile = new File(target, name);

                // create directories
                final File parent = nextFile.getParentFile();
                if (parent != null) {
                    parent.mkdirs();
                }

                // write file
                try (OutputStream targetStream = new FileOutputStream(nextFile)) {
                    copy(zipStream, targetStream);
                }
            }
        }
    }

    public static byte[] getFileBytes(FileInputStream targetZipInputStream, String file) throws IOException {
        final ZipInputStream zipStream = new ZipInputStream(targetZipInputStream);
        ZipEntry nextEntry;
        while ((nextEntry = zipStream.getNextEntry()) != null) {
            final String name = nextEntry.getName();

            if (name.equals(file)) {
                ByteArrayOutputStream fos = new ByteArrayOutputStream();
                byte[] data = extractEntry(nextEntry, zipStream);

                return data;

            }

        }

        return null;
    }

    private static byte[] extractEntry(final ZipEntry entry, InputStream is) throws IOException {
        ByteArrayOutputStream fos = new ByteArrayOutputStream();
        try {
            fos = new ByteArrayOutputStream();
            final byte[] buf = new byte[8192];
            int read = 0;
            int length;

            while ((length = is.read(buf)) >= 0) {
                fos.write(buf, 0, length);
            }

            return fos.toByteArray();
        } catch (IOException ioex) {
            ioex.printStackTrace();
        }

        return null;
    }

    private static void copy(final InputStream source, final OutputStream target) throws IOException {
        final int bufferSize = 4 * 1024;
        final byte[] buffer = new byte[bufferSize];

        int nextCount;
        while ((nextCount = source.read(buffer)) >= 0) {
            target.write(buffer, 0, nextCount);
        }
    }
}