package de.gjf.util;

import java.util.Date;

public class TimeHelper {

    public static long getTimestamp() {
        Date date = new Date();
        return date.getTime();
    }

}
