package de.gjf.schedule;

import de.gjf.module.ApplicationModule;

public class Schedule {

    public static void runAsyncDelay(ApplicationModule applicationModule, Runnable runnable, int delay) {
        ScheduleInstance scheduleInstance = new ScheduleInstance();
        scheduleInstance.thread =
        new Thread(() -> {
            try {
                Thread.sleep(delay * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            runnable.run();
        });
        scheduleInstance.thread.start();

        applicationModule.getFramework().scheduleManager.registerInstance(scheduleInstance);
    }

    public static void runAsyncDelayTimed(ApplicationModule applicationModule, Runnable runnable, int delay, int time) {
        ScheduleInstance scheduleInstance = new ScheduleInstance();
        scheduleInstance.thread =
        new Thread(() -> {
            try {
                Thread.sleep(delay * 1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            while (scheduleInstance.active) {
                runnable.run();
                try {
                    Thread.sleep(time * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        scheduleInstance.thread.start();

        applicationModule.getFramework().scheduleManager.registerInstance(scheduleInstance);
    }

}
