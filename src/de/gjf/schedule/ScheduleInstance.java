package de.gjf.schedule;

import de.gjf.module.ApplicationModule;

public class ScheduleInstance {

    public Thread thread = null;
    public boolean active = true;
    public ApplicationModule applicationModule = null;

    public void stop() {
        this.active = false;
        thread.interrupt();
    }

}
