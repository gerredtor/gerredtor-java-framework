package de.gjf.schedule;

import de.gjf.module.ApplicationModule;

import java.util.ArrayList;

public class ScheduleManager {

    private final ApplicationModule applicationModule;

    private ArrayList<ScheduleInstance> scheduleInstances = new ArrayList<>();

    public ScheduleManager(ApplicationModule applicationModule) {
        this.applicationModule = applicationModule;
    }

    public void registerInstance(ScheduleInstance scheduleInstance) {
        this.scheduleInstances.add(scheduleInstance);
    }

    public void stop(ApplicationModule applicationModule) {
        for (ScheduleInstance scheduleInstance : this.scheduleInstances) {
            if (scheduleInstance.applicationModule.equals(applicationModule)) {
                scheduleInstance.stop();
                this.scheduleInstances.remove(scheduleInstance);
            }
        }
    }
}
