package de.gjf.command;

public class Command {

	public final String cmd;
	public final String[] args;
	public final String input;
	
	public Command(String cmd, String[] args, String input) {
		this.cmd = cmd;
		this.args = args;
		this.input = input;
	}
	
}
