package de.gjf.command;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;

import de.gjf.module.ApplicationModule;

public class CommandManager {

	private final ApplicationModule applicationModule;
	
	private HashMap<String, Action> commandActions = new HashMap<String, Action>();
	
	public CommandManager(ApplicationModule applicationModule) {
		this.applicationModule = applicationModule;
	}
	
	public void registerCommand(String command, Action action) {
		this.commandActions.put(command, action);
	}
	
	public void unregisterCommand(String command) {
		this.commandActions.remove(command);
	}
	
	public void checkConsoleInput(String input) {
		String[] inputArr = this.getInputArr(input);
		checkIsCommandRegistred(inputArr[0], input);
	}
	
	public void checkConsoleInputAsync(String input) {
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				String[] inputArr = getInputArr(input);
				checkIsCommandRegistred(inputArr[0], input);
			}
			
		});
	}
	
	private void checkIsCommandRegistred(String command, String input) {
		if (this.commandActions.containsKey(command)) {
			Action action = this.commandActions.get(command);
			
			if (!this.checkRequirements(input, action.commandRequire)) {
				return;
			}
			
			if (action.actionType.equals(ActionType.INVOKE_CALL)) {
				try {
					if (action.invokeMethod.getParameterCount() >= 1) {
						action.invokeMethod.invoke(action.invokeClass, new Object[] {this.getCommand(input)});
					} else {
						action.invokeMethod.invoke(action.invokeClass);
					}
				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
					e.printStackTrace();
				}
			} else if (action.actionType.equals(ActionType.RUNNABLE_CALL)) {
				new Thread(action.runnable).start();
			} else if (action.actionType.equals(ActionType.FIRE_EVENT)) {
				this.applicationModule.getGJF().getEventManager().fireEvent(action.fireEvent);
			}
		}
	}
	
	private boolean checkRequirements(String input, CommandRequire commandRequire) {
		if (commandRequire == null) {
			return true;
		}
		
		Command command = this.getCommand(input);
		
		//Check args lenght
		if (!(commandRequire.maxArgs == 0 && commandRequire.minArgs == commandRequire.maxArgs)) {
			if (command.args.length > commandRequire.maxArgs || command.args.length < commandRequire.minArgs) {
				return false;
			}
		}
		
		//Check args
		if (commandRequire.args != null) {
			for (int i = 0; i < command.args.length; i++) {
				if (!command.args[i].equalsIgnoreCase(commandRequire.args[i])) {
					return false;
				}
			}
		}
		
		return true;
	}
	
	private Command getCommand(String input) {
		String[] inputArr = input.split(" ");
		
		String cmd = inputArr[0];
		inputArr[0] = null;
		String[] args = inputArr;
		
		return new Command(cmd, args, input);
	}
	
	private String[] getInputArr(String input) {
		return input.split(" ");
	}
}
