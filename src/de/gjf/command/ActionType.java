package de.gjf.command;

public enum ActionType {
	RUNNABLE_CALL, INVOKE_CALL, FIRE_EVENT
}
