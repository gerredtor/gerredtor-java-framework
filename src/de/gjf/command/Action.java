package de.gjf.command;

import java.lang.reflect.Method;

import de.gjf.event.Event;

public class Action {
	
	protected final ActionType actionType;
	protected final Runnable runnable;
	protected final Method invokeMethod;
	protected final Class<CommandListener> invokeClass;
	protected final Event fireEvent;
	
	protected CommandRequire commandRequire = null;
	
	public Action(Runnable runnable) {
		this.actionType = ActionType.RUNNABLE_CALL;
		this.runnable = runnable;
		this.invokeMethod = null;
		this.invokeClass = null;
		this.fireEvent = null;
	}
	
	public Action(Method invokeMethod, Class<CommandListener> invokeClass) {
		this.actionType = ActionType.INVOKE_CALL;
		this.invokeMethod = invokeMethod;
		this.invokeClass = invokeClass;
		this.runnable = null;
		this.fireEvent = null;
	}
	
	public Action(Event fireEvent) {
		this.actionType = ActionType.FIRE_EVENT;
		this.fireEvent = fireEvent;
		this.runnable = null;
		this.invokeMethod = null;
		this.invokeClass = null;
	}
	
	public void setCommandRequire(CommandRequire commandRequire) {
		this.commandRequire = commandRequire;
	}
	
}
