package de.gjf.command;

public class CommandRequire {

	public int minArgs = 0;
	public int maxArgs = 0;
	
	public String[] args = null;
	
	public CommandRequire(int minArgs, int maxArgs, String[] args) {
		this.minArgs = minArgs;
		this.maxArgs = maxArgs;
		
		this.args = args;
	}
	
}
