package de.gjf.configuration;

import java.io.File;
import java.io.IOException;

import de.gjf.util.FileHelper;

public class ConfigurationManager {

	private static String getDefaultJson(String file) {
		try {
			String json = FileHelper.getFileFromResources(file);
			return json;
		} catch (IOException e) { e.printStackTrace(); return null; }
	}

	public static Configuration createConfiguration(Configuration configuration) {
		ConfigurationOptions co = new ConfigurationOptions();
		co.forceWriteEmptyJsonFile = true;

		return ConfigurationManager.createConfiguration(configuration, co);
	}

	public static Configuration createConfiguration(Configuration configuration, ConfigurationOptions configurationOptions) {

		//Write empty File
		if (configurationOptions.forceWriteEmptyJsonFile && configurationOptions.file != null) {
			FileHelper fh = new FileHelper();
			
			try {
				fh.writeEmptyJsonFile(configurationOptions.file);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		//Copy Defaults
		if (configurationOptions.copyDefaults && configurationOptions.file != null && configurationOptions.defaultFile != null) {
			FileHelper fh = new FileHelper();

			try {
				fh.writeFile(configurationOptions.file, ConfigurationManager.getDefaultJson(configurationOptions.defaultFile));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return configuration;
	}
}
