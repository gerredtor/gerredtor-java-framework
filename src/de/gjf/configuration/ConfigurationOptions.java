package de.gjf.configuration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;

import de.gjf.GJF;
import de.gjf.configuration.types.JsonConfiguration;

public class ConfigurationOptions {

	public boolean copyDefaults = false;
	public String fileDefaultUrl = null;
	public GJF gjf = null;

	public String envFile = "env.json";

	//
	
	public boolean forceWriteEmptyJsonFile = false;
	public String file = null;

	public boolean MVCActive = true;

	public String defaultFile = null;

	public void copyFileFromResources(GJF gjf, String file, String destination) {
		System.out.println("Kopiere Config...");
		
		InputStream is = gjf.getClass().getResourceAsStream(file);
		
		System.out.println("copy");
		
		try {
			FileUtils.copyInputStreamToFile(is, new File(destination));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("sss" + destination);
		
		System.out.println("Fertig Kopiert...");
	}
		
}
