package de.gjf.configuration;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class Configuration extends Base {

	public Configuration() {
	}

	public Configuration(File file) {
		super(file);
	}
	
	public Configuration(File file, boolean copyDefault) {
		super(file, copyDefault);
	}
	
	/*public abstract void set(Object key, Object value);
	
	public abstract void put(Object key, Object value);
	
	public abstract Object get(Object key);
	public abstract JSONObject getJsonObject(Object key);

	public abstract String getString(Object key);
	public abstract Integer getInteger(Object key);
	public abstract Long getLong(Object key);
	
	public abstract HashMap<Object, Object> getMap();
	
	public abstract void save();
	public abstract void save(File file);
	
	public abstract void load();
	public abstract void load(File file);
	
	public abstract void initMap();

	public abstract String serialization(Object object);
    public abstract <T> Object deserializationArray(String json, Class<T[]> clazz);
	public abstract <T> Object deserializationClass(String json, Class<T> clazz);
*/

	//Getter
	public abstract Object get(String key);
	public abstract String getString(String key);
	public abstract int getInt(String key);
	public abstract boolean getBoolean(String key);
	public abstract Long getLong(String key);
	public abstract Double getDouble(String key);
	public abstract <T> Object deserializationClass(String key, Class<T> clazz);
	public abstract <T> Object deserializationArray(String json, Class<T[]> clazz);

	//Setter
	public abstract void set(String key, Object o);
	public abstract void setString(String key);
	public abstract void setInt(String key);
	public abstract void setBoolean(String key);
	public abstract void setLong(String key);
	public abstract void setDouble(String key);
	public abstract String serialization(Object object);

	//
	public void save() {

	}
}
