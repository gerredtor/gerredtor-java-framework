package de.gjf.configuration.types;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.gjf.configuration.Configuration;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class YamlConfiguration extends Configuration {

    private JSONObject jsonObject = new JSONObject();
    private HashMap<String, Object> map = new HashMap<String, Object>();

    public YamlConfiguration(byte[] data) {
        this.load(data);
    }

    public YamlConfiguration(File file) {
        super(file);

        this.load(file);
    }

    public YamlConfiguration(File file, boolean copyDefault) {
        super(file, copyDefault);

        this.load(file);
    }

    public YamlConfiguration() {
    }

    
    public void set(Object key, Object value) {
        this.jsonObject.put((String) key, value);
    }

    
    public Object get(Object key) {
        Object ob = null;

        try {
            ob = this.jsonObject.get((String)key);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

        return ob;
    }

    
    public JSONObject getJsonObject(Object key) {
        JSONObject jo = null;

        try {
            jo = this.jsonObject.getJSONObject(key.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return jo;
    }

    
    public String getString(Object key) {
        return (String) this.get(key);
    }

    
    public Integer getInteger(Object key) {
        return (Integer) this.get(key);
    }

    
    public HashMap<Object, Object> getMap() {
        return null;
    }

    public HashMap<String, Object> getHashMap() {
        return toMap(this.jsonObject);
    }

    private static HashMap<String, Object> toMap(JSONObject object) throws JSONException {
        HashMap<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while(keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    private static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for(int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if(value instanceof JSONArray) {
                value = toList((JSONArray) value);
            }

            else if(value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }

    
    public void save() {
        try {
            FileUtils.writeStringToFile(this.file, this.jsonObject.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    
    public void save(File file) {
        this.file = file;
        this.save();
    }

    
    public void load() {
        try {
            String content = FileUtils.readFileToString(this.file);

            this.jsonObject = new JSONObject(content);

            if (this.jsonObject == null) {
                this.jsonObject = new JSONObject();
            }

            //this.replaceTags();
        } catch (IOException e) {
            this.jsonObject = new JSONObject();
        }
    }

    private void load(byte[] data) {
        String content = new String(data);

        this.jsonObject = new JSONObject(content);

        if (this.jsonObject == null) {
            this.jsonObject = new JSONObject();
        }
    }

    
    public void load(File file) {
        this.file = file;
        this.load();
    }

    
    public void initMap() {
        this.jsonObject = new JSONObject();
        this.map = new HashMap<>();
    }

    
    public String serialization(Object object) {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

        return gson.toJson(object);
    }

    
    public <T> Object deserializationArray(String json, Class<T[]> clazz) {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

        return Arrays.asList(gson.fromJson(json, clazz));
    }

    
    public Object get(String key) {
        return null;
    }

    
    public String getString(String key) {
        return null;
    }

    
    public int getInt(String key) {
        return 0;
    }

    
    public boolean getBoolean(String key) {
        return false;
    }

    
    public Long getLong(String key) {
        return null;
    }

    
    public Double getDouble(String key) {
        return null;
    }

    
    public <T> Object deserializationClass(String json, Class<T> clazz) {
        Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();

        return gson.fromJson(json, clazz);
    }

    
    public void set(String key, Object o) {

    }

    
    public void setString(String key) {

    }

    
    public void setInt(String key) {

    }

    
    public void setBoolean(String key) {

    }

    
    public void setLong(String key) {

    }

    
    public void setDouble(String key) {

    }

    
    public void put(Object key, Object value) {
        this.jsonObject.put((String) key, value);
    }

    
    public Long getLong(Object key) {
        return (Long) this.get(key);
    }

	/*@SuppressWarnings("unused")
	private void replaceTags() {
		for(Map.Entry<Object, Object> entry : this.yamlMap.entrySet()) {
			Object key = entry.getKey();
			Object value = entry.getValue();

			if (value.toString().startsWith("{$") && value.toString().endsWith("}")) {
				String id = value.toString().replace("{$", "").replace("}", "");

				if (!this.yamlMap.containsKey(id)) {
					//Aniflix.consoleOut("Replace key not exists : " + id, "[YamlConfiguration]");
					return;
				}

				Object idValue = this.get("id");
				this.put(id, idValue);
			}
		}
	}*/

}
