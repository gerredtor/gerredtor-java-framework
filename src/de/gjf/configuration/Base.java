package de.gjf.configuration;

import java.io.File;

public class Base {

	protected File file = null;

	private boolean copyDefault = false;
	
	public Base() {
	}
	
	public Base(File file) {
		this.file = file;
	}
	
	public Base(File file, boolean copyDefault) {
		this.copyDefault = copyDefault;
		this.file = file;
	}
		
	
}
