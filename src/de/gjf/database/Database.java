package de.gjf.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

public class Database {

	private String host = null;
	private String user = null;
	private String password = null;
	private String database = null;
	
	public static String CONFIG_DATABASE_HOST_PREFIX = "HOST";
	public static String CONFIG_DATABASE_USER_PREFIX = "USER";
	public static String CONFIG_DATABASE_PASSWORD_PREFIX = "PASSWORD";
	public static String CONFIG_DATABASE_DATABASE_PREFIX = "DATABASE";
	
	private Connection connection = null;
	
	public Database(String host, String user, String password, String database) {
		this.host = host;
		this.user = user;
		this.password = password;
		this.database = database;
		
		this.connect();
	}
	
	public Database(String configDatabaseKey) {
		this.connect();
	}
	
	public int getInt(String query) {
		return (Integer) this.get(query);
	}
	
	public String getString(String query) {
		return (String) this.get(query);
	}
	
	public Object get(String query) {
		try {
			Statement st = this.connection.createStatement();
			ResultSet rs = st.executeQuery(query);
			
			return rs.getObject(0);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	protected void connect() {
		try {
			try {
				Class.forName("com.mysql.jdbc.Driver");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				return;
			}
			
			this.connection = DriverManager.getConnection("jdbc:mysql://"+this.host+"/"+this.database+"?"
		            + "user="+this.user+"&password="+this.password);

	    } catch (SQLException ex) {
	    	//Cannot connect
	    	System.out.println("Cannot connect");
	    } 
	}
		
	protected ResultSet query(String query) {
		ResultSet rs = null;
		
		try {
			Statement st = (Statement) this.connection.createStatement();
			rs = st.executeQuery(query);
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}
		
		return rs;
	}
	
	protected int update(String query) {
		int rs = 0;
		
		try {
			Statement st = (Statement) this.connection.createStatement();
			rs = st.executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
			return 0;
		}
		
		return rs;
	}
	
	public ArrayList<HashMap<Object, Object>> getArray(String q) {
		if (this.connection == null) {
			return null;
		}
		
		ArrayList<HashMap<Object, Object>> rows = new ArrayList<HashMap<Object, Object>>();
		
		Statement st = null;
		ResultSet rs = null;
		ResultSetMetaData rsmd = null;
		int columnCount = 0;
		
		try {
			st = this.connection.createStatement();
			rs = st.executeQuery(q);
			rsmd = rs.getMetaData();
			columnCount = rsmd.getColumnCount();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			while(rs.next())
			{
				HashMap<Object, Object> data = new HashMap<Object, Object>();
				
				for (int i = 0; i != columnCount-1; i++) {
					data.put(rsmd.getColumnLabel(i), rs.getObject(i));
				}
				
				rows.add(data);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return rows;
	}

	public void delete(String query) {
		try {
			this.connection.createStatement().executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void insert(String query) {
		try {
			this.connection.createStatement().executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void replace(String query) {
		try {
			this.connection.createStatement().executeUpdate(query);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
		
}
