import de.gjf.GJF;
import de.gjf.configuration.ConfigurationOptions;

public class main extends GJF {

    public main(ConfigurationOptions configurationOptions) {
        super(configurationOptions);
    }

    public static void main(String[] args) {
        ConfigurationOptions configurationOptions = new ConfigurationOptions();
        configurationOptions.copyDefaults = false;
        configurationOptions.envFile = "env.json";

        new main(configurationOptions);
    }
}
